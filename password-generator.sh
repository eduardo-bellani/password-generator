# the following 2 are deprecated.

# useful to generate a password based on hexadecimal values
get_password(){
        unset -v password # make sure it's not exported
        set +o allexport  # make sure variables are not automatically exported
        IFS= read -rs password < /dev/tty && printf '%s' "$password" | sha256sum | awk '{printf "%s", $1}'
}

# same as get_password, but with decimal values
get_dec_password(){
	export BC_LINE_LENGTH=0
	get_password | tr a-z A-Z | echo "ibase=16; $(cat -)" | bc | xargs printf "%s"
}

# more secure system below. Use this instead

make_random() {
    # Optionaly, takes the desired length of the random string as argument
    head /dev/urandom | tr -dc A-Za-z0-9 | head -c"${1:-8}" | xargs echo
}

make_pw() {
    local desired_password_length=100
    local pw=''
    local salt=''
    read -rsp "Password:" pw
    read -rsp"
Salt:" salt
    echo -n "$pw" | nettle-pbkdf2 $salt -l$desired_password_length | tr -d '[:space:]'
}

make_pw_dec() {
    make_pw | tr a-z A-Z | echo "ibase=16; $(cat -)" | BC_LINE_LENGTH=0 bc
}
